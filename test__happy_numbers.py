#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

from happy_numbers import happy_number, main


class HappyNumbersTestCase(unittest.TestCase):
    """Tests for the happy_numbers script."""

    def test_happy_number(self):
        self.assertTrue(happy_number(7))

    def test_sad_number(self):
        self.assertFalse(happy_number(8))

if __name__ == '__main__':
    unittest.main()
