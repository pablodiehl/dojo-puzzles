#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This is script is going to solve the following challenge:
# (found at http://dojopuzzles.com/problemas/exibe/numeros-felizes/)
#
# A happy number is defined by the following process: Starting with any
# positive integer, replace the number by the sum of the squares of its digits
# in base-ten, and repeat the process until the number either equals 1 (where
# it will stay), or it loops endlessly in a cycle that does not include 1.
# Those numbers for which this process ends in 1 are happy numbers, while those
# that do not end in 1 are unhappy numbers (or sad numbers).
#
# For example, 7 is a happy number:
# 7² = 49
# 4² + 9² = 97
# 9² + 7² = 130
# 1² + 3² + 0² = 10
# 1² + 0² = 1

import sys


def main():
    """Extracts the argument sent from the user
    and tries to verify if the number is happy.
    """
    input_arg = ''
    if len(sys.argv) > 1:
        input_arg = sys.argv[1]

    if input_arg.isdigit():
        print(happy_number(input_arg))
    else:
        print ("I was expecting a number, but you didn't send me a number, "
               "so I got an headache and must leave now. Bye, bye!")


def happy_number(current_number, previous_numbers=[]):
    """Verifies if a given number is happy or not."""
    digits = [int(i) for i in str(current_number)]
    sum_of_powers = 0

    for digit in digits:
        sum_of_powers += digit ** 2

    if sum_of_powers == 1:
        return True
    elif sum_of_powers in previous_numbers:
        # If we already had gone through a number,
        # it means the number is sad ='(
        return False

    previous_numbers.append(sum_of_powers)

    return happy_number(sum_of_powers, previous_numbers)

if __name__ == '__main__':
    main()
